import java.util.Random;

public class Cat extends Animals {

	static String[] catSounds = {"Miaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

	public Cat(String nama, int Panjang) {
		super(nama, Panjang);
	}

	public void getBrushed() {
		System.out.format("Time to clean %s's fur \n", this.getNama());
		System.out.format("%s makes a voice: Nyaaan... \n", this.getNama());
		System.out.println("Back to the office!");
	}

	public void getCuddled() {
		Random random = new Random();
		int num = random.nextInt(catSounds.length);
		System.out.format("%s makes a voice: %s \n", this.getNama(), catSounds[num]);
		System.out.println("Back to the office!"); 
	}
}