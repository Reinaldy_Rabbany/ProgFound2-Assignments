import java.util.*;

public class JavariPark {
	public static void main(String[] args) {

		String[] animalArr = {"cat", "lion", "eagle", "parrot", "hamster"};

		ArrayList<String> arrNamaKucing = new ArrayList<String>();
		ArrayList<Cat> arrCat = new ArrayList<Cat>();

		ArrayList<String> arrNamaElang = new ArrayList<String>();
		ArrayList<Eagle> arrEagle = new ArrayList<Eagle>();

		ArrayList<String> arrNamaHamster = new ArrayList<String>();
		ArrayList<Hamster> arrHamster = new ArrayList<Hamster>();

		ArrayList<String> arrNamaSinga = new ArrayList<String>();
		ArrayList<Lion> arrLion = new ArrayList<Lion>();

		ArrayList<String> arrNamaBeo = new ArrayList<String>();
		ArrayList<Parrot> arrParrot = new ArrayList<Parrot>();

		System.out.println("Welcome to Javari Park!");

		System.out.println("Input the number of animals");

		for (String animal : animalArr) {

			System.out.print(animal + ": ");
			Scanner input1 = new Scanner(System.in);
			int animalNum = input1.nextInt();

			if (animalNum > 0) {
				System.out.format("Provide the information of %s(s): \n", animal);
				Scanner input2 = new Scanner(System.in);
				String animalInfo = input2.nextLine();
				if (animal.equals(animalArr[0])) {
					for (int i = 0; i < animalNum; i++) {
						String animalInfo2 = animalInfo.split(",")[i];
						String animalInfo3 = animalInfo2.split("\\|")[0];
						arrNamaKucing.add(animalInfo3);
						String animalInfo4 = animalInfo2.split("\\|")[1];
						int intPanjang = Integer.parseInt(animalInfo4);
						Cat newCat = new Cat(animalInfo3, intPanjang);
						arrCat.add(newCat);
					}
				}
				else if (animal.equals(animalArr[1])) {
					for (int i = 0; i < animalNum; i++) {
						String animalInfo2 = animalInfo.split(",")[i];
						String animalInfo3 = animalInfo2.split("\\|")[0];
						arrNamaSinga.add(animalInfo3);
						String animalInfo4 = animalInfo2.split("\\|")[1];
						int intPanjang = Integer.parseInt(animalInfo4);
						Lion newLion = new Lion(animalInfo3, intPanjang);
						arrLion.add(newLion);
					}
				}
				else if (animal.equals(animalArr[2])) {
					for (int i = 0; i < animalNum; i++) {
						String animalInfo2 = animalInfo.split(",")[i];
						String animalInfo3 = animalInfo2.split("\\|")[0];
						arrNamaElang.add(animalInfo3);
						String animalInfo4 = animalInfo2.split("\\|")[1];
						int intPanjang = Integer.parseInt(animalInfo4);
						Eagle newEagle = new Eagle(animalInfo3, intPanjang);
						arrEagle.add(newEagle);
					}
				}
				else if (animal.equals(animalArr[3])) {
					for (int i = 0; i < animalNum; i++) {
						String animalInfo2 = animalInfo.split(",")[i];
						String animalInfo3 = animalInfo2.split("\\|")[0];
						arrNamaBeo.add(animalInfo3);
						String animalInfo4 = animalInfo2.split("\\|")[1];
						int intPanjang = Integer.parseInt(animalInfo4);
						Parrot newParrot = new Parrot(animalInfo3, intPanjang);
						arrParrot.add(newParrot);
					}
				}
				else {
					for (int i = 0; i < animalNum; i++) {
						String animalInfo2 = animalInfo.split(",")[i];
						String animalInfo3 = animalInfo2.split("\\|")[0];
						arrNamaHamster.add(animalInfo3);
						String animalInfo4 = animalInfo2.split("\\|")[1];
						int intPanjang = Integer.parseInt(animalInfo4);
						Hamster newHamster = new Hamster(animalInfo3, intPanjang);
						arrHamster.add(newHamster);
					}
				}
			}
		}

		System.out.println("Animals have been successfully recorded!" + "\n");

		System.out.println("=============================================");
		System.out.println("Cage arrangement:");

		if (arrCat.size() > 0) {
			ArrayList<ArrayList<Cat>> list = Cages.catToCage(arrCat);
			Cages.printCatCage(list);
			ArrayList<ArrayList<Cat>> reList = Cages.rearrangeCatCage(list);
			System.out.println("After rearrangement...");
			Cages.printCatCage(reList);
		}
		if (arrLion.size() > 0) {
			ArrayList<ArrayList<Lion>> list = Cages.lionToCage(arrLion);
			Cages.printLionCage(list);
			ArrayList<ArrayList<Lion>> reList = Cages.rearrangeLionCage(list);
			System.out.println("After rearrangement...");
			Cages.printLionCage(reList);
		}
		if (arrEagle.size() > 0) {
			ArrayList<ArrayList<Eagle>> list = Cages.eagleToCage(arrEagle);
			Cages.printEagleCage(list);
			ArrayList<ArrayList<Eagle>> reList = Cages.rearrangeEagleCage(list);
			System.out.println("After rearrangement...");
			Cages.printEagleCage(reList);
		}
		if (arrParrot.size() > 0) {
			ArrayList<ArrayList<Parrot>> list = Cages.parrotToCage(arrParrot);
			Cages.printParrotCage(list);
			ArrayList<ArrayList<Parrot>> reList = Cages.rearrangeParrotCage(list);
			System.out.println("After rearrangement...");
			Cages.printParrotCage(reList);
		}
		if (arrHamster.size() > 0) {
			ArrayList<ArrayList<Hamster>> list = Cages.hamsterToCage(arrHamster);
			Cages.printHamsterCage(list);
			ArrayList<ArrayList<Hamster>> reList = Cages.rearrangeHamsterCage(list);
			System.out.println("After rearrangement...");
			Cages.printHamsterCage(reList);
		}


		System.out.println("NUMBER OF ANIMALS:");
		System.out.println("cat: " + arrCat.size());
		System.out.println("lion: " + arrLion.size());
		System.out.println("parrot: " + arrParrot.size());
		System.out.println("eagle: " + arrEagle.size());
		System.out.println("hamster: " + arrHamster.size() + "\n");

		System.out.println("=============================================");

		String[] animalArr2 = {"cat", "eagle", "hamster", "parrot", "lion"};

		int choosenInt = 0;
		while (choosenInt != 99) {
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
			Scanner choosen = new Scanner(System.in);
			choosenInt = choosen.nextInt();
			if (choosenInt == 99) {
				break;
			}
			else if((choosenInt > 5 && choosenInt < 99) || choosenInt > 99 || choosenInt < 1) {
				System.out.println("Please input the number correctly\n");
				continue;
			}
			System.out.format("Mention the name of %s you want to visit: ", animalArr2[choosenInt - 1]);
			Scanner name = new Scanner(System.in);
			String animalName = name.nextLine();

			if (choosenInt == 1) {
				if (arrNamaKucing.indexOf(animalName) == -1) {
					System.out.println("There is no cat with that name! Back to the office! \n");
					continue;
				}
				else{
					int nums = arrNamaKucing.indexOf(animalName);
					Cat cat = arrCat.get(nums);
					System.out.format("You are visitting %s (cat) now, what would you like to do? \n", animalName);
					System.out.println("1: Brush the fur 2: Cuddle");
					Scanner numOption = new Scanner(System.in);
					int num = numOption.nextInt();
					if (num == 1){
						cat.getBrushed();
					}
					else{
						cat.getCuddled();
					}
				}
				System.out.println();
				continue;
			}
			else if (choosenInt == 2) {
				if (arrNamaElang.indexOf(animalName) == -1) {
					System.out.println("There is no eagle with that name! Back to the office! \n");
					continue;
				}
				else{
					int nums = arrNamaElang.indexOf(animalName);
					Eagle eagle = arrEagle.get(nums);
					System.out.format("You are visitting %s (eagle) now, what would you like to do? \n", animalName);
					System.out.println("1: Order to fly");
					Scanner numOption = new Scanner(System.in);
					int num = numOption.nextInt();
					if (num == 1){
						eagle.flyingEagle();
					}
					else{
						eagle.doNothing();
					}
				}
				System.out.println();
				continue;
			}
			else if (choosenInt == 3) {
				if (arrNamaHamster.indexOf(animalName) == -1) {
					System.out.println("There is no hamster with that name! Back to the office! \n");
					continue;
				}
				else{
					int nums = arrNamaHamster.indexOf(animalName);
					Hamster hamster = arrHamster.get(nums);
					System.out.format("You are visitting %s (hamster) now, what would you like to do? \n", animalName);
					System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
					Scanner numOption = new Scanner(System.in);
					int num = numOption.nextInt();
					if (num == 1){
						hamster.gnawing();
					}
					else{
						hamster.running();
					}
				}
				System.out.println();
				continue;
			}
			else if (choosenInt == 4) {
				if (arrNamaBeo.indexOf(animalName) == -1) {
					System.out.println("There is no parrot with that name! Back to the office! \n");
					continue;
				}
				else{
					int nums = arrNamaBeo.indexOf(animalName);
					Parrot parrots = arrParrot.get(nums);
					System.out.format("You are visitting %s (parrot) now, what would you like to do? \n", animalName);
					System.out.println("1: Order to fly 2: Do conversation");
					Scanner numOption = new Scanner(System.in);
					int num = numOption.nextInt();
					if (num == 1){
						parrots.flyingParrot();
					}
					else{
						parrots.doConversation();
					}
				}
				System.out.println();
				continue;
			}
			else if (choosenInt == 5) {
				if (arrNamaSinga.indexOf(animalName) == -1) {
					System.out.println("There is no lion with that name! Back to the office! \n");
					continue;
				}
				else{
					int nums = arrNamaSinga.indexOf(animalName);
					Lion lion = arrLion.get(nums);
					System.out.format("You are visitting %s (lion) now, what would you like to do? \n", animalName);
					System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
					Scanner numOption = new Scanner(System.in);
					int num = numOption.nextInt();
					if (num == 1){
						lion.Hunting();
					}
					else if(num == 2) {
						lion.brushTheMane();
					}
					else{
						lion.disturbLion();
					}
				}
				System.out.println();
				continue;
			}
		}
	}
}