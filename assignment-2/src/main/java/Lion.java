public class Lion extends Animals {

	public Lion(String nama, int panjang) {
		super(nama, panjang);
	}

	public void Hunting() {
		System.out.println("Lion is hunting..");
		System.out.format("%s makes a voice: err...! \n", this.getNama());
		System.out.println("Back to the office!");
	}

	public void brushTheMane() {
		System.out.println("Clean the Lion's mane");
		System.out.format("%s makes a voice: Hauhhmm! \n", this.getNama());
		System.out.println("Back to the office!");
	}

	public void disturbLion() {
		System.out.format("%s makes a voice: HAUHHMM! \n", this.getNama());
		System.out.println("Back to the office!");
	}
}