import java.util.*;

public class Animals {

	protected String nama;
	protected int panjang;
	protected String tipe;

	public Animals(String nama, int panjang) {
		this.nama = nama;
		this.panjang = panjang;
	}

	public String getNama() {
		return nama;
	}
	public int getPanjang() {
		return panjang;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe2) {
		tipe = tipe2;
	}
}