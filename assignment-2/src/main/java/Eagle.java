public class Eagle extends Animals {

	public Eagle(String nama, int panjang) {
		super(nama,panjang);
	}

	public void flyingEagle() {
		System.out.format("%s makes a voice: kwaakk... \n", this.getNama());
		System.out.println("You hurt!");
		System.out.println("Back to the office!");
	}

	public void doNothing() {
		System.out.println("You do nothing...");
		System.out.println("Back to the office!");
	}
}