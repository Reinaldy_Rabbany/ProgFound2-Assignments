import java.util.*;

public class Cages {

	protected static String type;

	public static ArrayList<ArrayList<Cat>> catToCage(ArrayList<Cat> cats) {
		type = "Indoor";
		System.out.println("location: " + type);

		ArrayList<ArrayList<Cat>> arr2Cat = new ArrayList<ArrayList<Cat>>();

		ArrayList<Cat> level1 = new ArrayList<Cat>();
		ArrayList<Cat> level2 = new ArrayList<Cat>();
		ArrayList<Cat> level3 = new ArrayList<Cat>();

		arr2Cat.add(level1);
		arr2Cat.add(level2);
		arr2Cat.add(level3);

		for (Cat cat : cats) {
			if (cat.getPanjang() < 45) {
				cat.setTipe("A"); 
			}
			else if (cat.getPanjang() >= 45 && cat.getPanjang() <= 60) {
				cat.setTipe("B");
			}
			else {
				cat.setTipe("C");
			}
		}

		if (cats.size() < 4) {
    		for (int i = 0; i < cats.size(); i++ ) {
      			ArrayList<Cat> level = new ArrayList<Cat>();
      			level.add(cats.get(i));
      			arr2Cat.set(i,level);
    		}
    		return arr2Cat;
  		}

		for (int i = 0; i < cats.size() / 3; i++) {
			level1.add(i, cats.get(i));
		}
		
		for (int i = cats.size() / 3; i < (cats.size() * 2) / 3; i++) {
			level2.add(i - (cats.size() / 3), cats.get(i));
		}

		for (int i = (cats.size() * 2) / 3; i < cats.size(); i++) {
			level3.add(i - (cats.size() * 2) / 3, cats.get(i));
		}

		arr2Cat.add(level1);
		arr2Cat.add(level2);
		arr2Cat.add(level3);

		return arr2Cat;
	}

	public static ArrayList<ArrayList<Cat>> rearrangeCatCage(ArrayList<ArrayList<Cat>> catCage) {
    	for (int i = 0; i < catCage.size(); i++ ) {
      		ArrayList<Cat> cats = new ArrayList<Cat>();
      		cats = catCage.get(i);
      		for (int j = 0; j < catCage.get(i).size(); j++) {
        		cats.add(j,cats.get(cats.size() - 1));
        		cats.remove(cats.size() - 1);
      		}
    	}
    	catCage.add(0,catCage.get(catCage.size()-1));
    	catCage.remove(catCage.size()-1);
    	return catCage;
  	}

	public static void printCatCage(ArrayList<ArrayList<Cat>> levelCage) {
		String temp = "";
    	for (int i = 2; i>-1; i--) {
      		temp = "";
      		ArrayList<Cat> animals = levelCage.get(i);
      		for (Cat animal : animals) {
        		temp += animal.getNama() + " (" + animal.getPanjang() + " - " + animal.getTipe() + ")" + ", ";
      		}
      		temp = "Level " + (i + 1) + ": " + temp + "\n";
      		System.out.print(temp);
      	}
      	System.out.println();
	}


	public static ArrayList<ArrayList<Lion>> lionToCage(ArrayList<Lion> lions) {
		type = "Outdoor";
		System.out.println("location: " + type);

		ArrayList<ArrayList<Lion>> arr2Lion = new ArrayList<ArrayList<Lion>>();

		ArrayList<Lion> level1 = new ArrayList<Lion>();
		ArrayList<Lion> level2 = new ArrayList<Lion>();
		ArrayList<Lion> level3 = new ArrayList<Lion>();

		arr2Lion.add(level1);
		arr2Lion.add(level2);
		arr2Lion.add(level3);

		for (Lion lion : lions) {
			if (lion.getPanjang() < 75) {
				lion.setTipe("A"); 
			}
			else if (lion.getPanjang() >= 75 && lion.getPanjang() <= 90) {
				lion.setTipe("B");
			}
			else {
				lion.setTipe("C");
			}
		}

		if (lions.size() < 4) {
    		for (int i = 0; i < lions.size(); i++ ) {
      			ArrayList<Lion> level = new ArrayList<Lion>();
      			level.add(lions.get(i));
      			arr2Lion.set(i,level);
    		}
    		return arr2Lion;
  		}

		for (int i = 0; i < lions.size() / 3; i++) {
			level1.add(i, lions.get(i));
		}
		for (int i = lions.size() / 3; i < (lions.size() * 2) / 3; i++) {
			level2.add(i - (lions.size() / 3), lions.get(i));
		}
		for (int i = (lions.size() * 2) / 3; i < lions.size(); i++) {
			level3.add(i - (lions.size() * 2) / 3, lions.get(i));
		}

		arr2Lion.add(level1);
		arr2Lion.add(level2);
		arr2Lion.add(level3);

		return arr2Lion;
	}

	public static ArrayList<ArrayList<Lion>> rearrangeLionCage(ArrayList<ArrayList<Lion>> lionCage) {
    	for (int i = 0; i < lionCage.size(); i++ ) {
      		ArrayList<Lion> lions = new ArrayList<Lion>();
      		lions = lionCage.get(i);
      		for (int j = 0; j < lionCage.get(i).size(); j++) {
        		lions.add(j,lions.get(lions.size() - 1));
        		lions.remove(lions.size() - 1);
      		}
    	}
    	lionCage.add(0,lionCage.get(lionCage.size()-1));
    	lionCage.remove(lionCage.size()-1);
    	return lionCage;
  	}

	public static void printLionCage(ArrayList<ArrayList<Lion>> levelCage) {
		String temp = "";
    	for (int i = 2; i>-1; i--) {
      		temp = "";
      		ArrayList<Lion> animals = levelCage.get(i);
      		for (Lion animal : animals) {
        		temp += animal.getNama() + " (" + animal.getPanjang() + " - " + animal.getTipe() + ")" + ", ";
      		}
      		temp = "Level " + (i + 1) + ": " + temp + "\n";
      		System.out.print(temp);
      	}
      	System.out.println();
	}

	public static ArrayList<ArrayList<Parrot>> parrotToCage(ArrayList<Parrot> parrots) {
		type = "Indoor";
		System.out.println("location: " + type);

		ArrayList<ArrayList<Parrot>> arr2Parrot = new ArrayList<ArrayList<Parrot>>();

		ArrayList<Parrot> level1 = new ArrayList<Parrot>();
		ArrayList<Parrot> level2 = new ArrayList<Parrot>();
		ArrayList<Parrot> level3 = new ArrayList<Parrot>();

		arr2Parrot.add(level1);
		arr2Parrot.add(level2);
		arr2Parrot.add(level3);

		for (Parrot parrot : parrots) {
			if (parrot.getPanjang() < 45) {
				parrot.setTipe("A"); 
			}
			else if (parrot.getPanjang() >= 45 && parrot.getPanjang() <= 60) {
				parrot.setTipe("B");
			}
			else {
				parrot.setTipe("C");
			}
		}

		if (parrots.size() < 4) {
    		for (int i = 0; i < parrots.size(); i++ ) {
      			ArrayList<Parrot> level = new ArrayList<Parrot>();
      			level.add(parrots.get(i));
      			arr2Parrot.set(i,level);
    		}
    		return arr2Parrot;
  		}

		for (int i = 0; i < parrots.size() / 3; i++) {
			level1.add(i, parrots.get(i));
		}
		for (int i = parrots.size() / 3; i < (parrots.size() * 2) / 3; i++) {
			level2.add(i - (parrots.size() / 3), parrots.get(i));
		}
		for (int i = (parrots.size() * 2) / 3; i < parrots.size(); i++) {
			level3.add(i - (parrots.size() * 2) / 3, parrots.get(i));
		}

		arr2Parrot.add(level1);
		arr2Parrot.add(level2);
		arr2Parrot.add(level3);

		return arr2Parrot;
	}

	public static ArrayList<ArrayList<Parrot>> rearrangeParrotCage(ArrayList<ArrayList<Parrot>> parrotCage) {
    	for (int i = 0; i < parrotCage.size(); i++ ) {
      		ArrayList<Parrot> parrots = new ArrayList<Parrot>();
      		parrots = parrotCage.get(i);
      		for (int j = 0; j < parrotCage.get(i).size(); j++) {
        		parrots.add(j,parrots.get(parrots.size() - 1));
        		parrots.remove(parrots.size() - 1);
      		}
    	}
    	parrotCage.add(0,parrotCage.get(parrotCage.size()-1));
    	parrotCage.remove(parrotCage.size()-1);
    	return parrotCage;
  	}

	public static void printParrotCage(ArrayList<ArrayList<Parrot>> levelCage) {
		String temp = "";
    	for (int i = 2; i>-1; i--) {
      		temp = "";
      		ArrayList<Parrot> animals = levelCage.get(i);
      		for (Parrot animal : animals) {
        		temp += animal.getNama() + " (" + animal.getPanjang() + " - " + animal.getTipe() + ")" + ", ";
      		}
      		temp = "Level " + (i + 1) + ": " + temp + "\n";
      		System.out.print(temp);
      	}
      	System.out.println();
	}

	public static ArrayList<ArrayList<Hamster>> hamsterToCage(ArrayList<Hamster> hamsters) {
		type = "Indoor";
		System.out.println("location: " + type);

		ArrayList<ArrayList<Hamster>> arr2Hamster = new ArrayList<ArrayList<Hamster>>();

		ArrayList<Hamster> level1 = new ArrayList<Hamster>();
		ArrayList<Hamster> level2 = new ArrayList<Hamster>();
		ArrayList<Hamster> level3 = new ArrayList<Hamster>();

		arr2Hamster.add(level1);
		arr2Hamster.add(level2);
		arr2Hamster.add(level3);

		for (Hamster hamster : hamsters) {
			if (hamster.getPanjang() < 45) {
				hamster.setTipe("A"); 
			}
			else if (hamster.getPanjang() >= 45 && hamster.getPanjang() <= 60) {
				hamster.setTipe("B");
			}
			else {
				hamster.setTipe("C");
			}
		}

		if (hamsters.size() < 4) {
    		for (int i = 0; i < hamsters.size(); i++ ) {
      			ArrayList<Hamster> level = new ArrayList<Hamster>();
      			level.add(hamsters.get(i));
      			arr2Hamster.set(i,level);
    		}
    		return arr2Hamster;
  		}

		for (int i = 0; i < hamsters.size() / 3; i++) {
			level1.add(i, hamsters.get(i));
		}
		for (int i = hamsters.size() / 3; i < (hamsters.size() * 2) / 3; i++) {
			level2.add(i - (hamsters.size() / 3), hamsters.get(i));
		}
		for (int i = (hamsters.size() * 2) / 3; i < hamsters.size(); i++) {
			level3.add(i - (hamsters.size() * 2) / 3, hamsters.get(i));
		}

		arr2Hamster.add(level1);
		arr2Hamster.add(level2);
		arr2Hamster.add(level3);

		return arr2Hamster;
	}

	public static ArrayList<ArrayList<Hamster>> rearrangeHamsterCage(ArrayList<ArrayList<Hamster>> hamsterCage) {
    	for (int i = 0; i < hamsterCage.size(); i++ ) {
      		ArrayList<Hamster> hamsters = new ArrayList<Hamster>();
      		hamsters = hamsterCage.get(i);
      		for (int j = 0; j < hamsterCage.get(i).size(); j++) {
        		hamsters.add(j,hamsters.get(hamsters.size() - 1));
        		hamsters.remove(hamsters.size() - 1);
      		}
    	}
    	hamsterCage.add(0,hamsterCage.get(hamsterCage.size()-1));
    	hamsterCage.remove(hamsterCage.size()-1);
    	return hamsterCage;
  	}

	public static void printHamsterCage(ArrayList<ArrayList<Hamster>> levelCage) {
		String temp = "";
    	for (int i = 2; i>-1; i--) {
      		temp = "";
      		ArrayList<Hamster> animals = levelCage.get(i);
      		for (Hamster animal : animals) {
        		temp += animal.getNama() + " (" + animal.getPanjang() + " - " + animal.getTipe() + ")" + ", ";
      		}
      		temp = "Level " + (i + 1) + ": " + temp + "\n";
      		System.out.print(temp);
      	}
      	System.out.println();
	}

	public static ArrayList<ArrayList<Eagle>> eagleToCage(ArrayList<Eagle> eagles) {
		type = "Outdoor";
		System.out.println("location: " + type);

		ArrayList<ArrayList<Eagle>> arr2Eagle = new ArrayList<ArrayList<Eagle>>();

		ArrayList<Eagle> level1 = new ArrayList<Eagle>();
		ArrayList<Eagle> level2 = new ArrayList<Eagle>();
		ArrayList<Eagle> level3 = new ArrayList<Eagle>();

		arr2Eagle.add(level1);
		arr2Eagle.add(level2);
		arr2Eagle.add(level3);

		for (Eagle eagle : eagles) {
			if (eagle.getPanjang() < 75) {
				eagle.setTipe("A"); 
			}
			else if (eagle.getPanjang() >= 75 && eagle.getPanjang() <= 90) {
				eagle.setTipe("B");
			}
			else {
				eagle.setTipe("C");
			}
		}

		if (eagles.size() < 4) {
    		for (int i = 0; i < eagles.size(); i++) {
      			ArrayList<Eagle> level = new ArrayList<Eagle>();
      			level.add(eagles.get(i));
      			arr2Eagle.set(i,level);
    		}
    		return arr2Eagle;
  		}

		for (int i = 0; i < eagles.size() / 3; i++) {
			level1.add(i, eagles.get(i));
		}
		for (int i = eagles.size() / 3; i < (eagles.size() * 2) / 3; i++) {
			level2.add(i - (eagles.size() / 3), eagles.get(i));
		}
		for (int i = (eagles.size() * 2) / 3; i < eagles.size(); i++) {
			level3.add(i - (eagles.size() * 2) / 3, eagles.get(i));
		}

		arr2Eagle.add(level1);
		arr2Eagle.add(level2);
		arr2Eagle.add(level3);

		return arr2Eagle;
	}

	public static ArrayList<ArrayList<Eagle>> rearrangeEagleCage(ArrayList<ArrayList<Eagle>> eagleCage) {
    	for (int i = 0; i < eagleCage.size(); i++ ) {
      		ArrayList<Eagle> eagles = new ArrayList<Eagle>();
      		eagles = eagleCage.get(i);
      		for (int j = 0; j < eagleCage.get(i).size(); j++) {
        		eagles.add(j,eagles.get(eagles.size() - 1));
        		eagles.remove(eagles.size() - 1);
      		}
    	}
    	eagleCage.add(0,eagleCage.get(eagleCage.size()-1));
    	eagleCage.remove(eagleCage.size()-1);
    	return eagleCage;
  	}

	public static void printEagleCage(ArrayList<ArrayList<Eagle>> levelCage) {
		String temp = "";
    	for (int i = 2; i>-1; i--) {
      		temp = "";
      		ArrayList<Eagle> animals = levelCage.get(i);
      		for (Eagle animal : animals) {
        		temp += animal.getNama() + " (" + animal.getPanjang() + " - " + animal.getTipe() + ")" + ", ";
      		}
      		temp = "Level " + (i + 1) + ": " + temp + "\n";
      		System.out.print(temp);
      	}
      	System.out.println();
	}
}