public class Hamster extends Animals {

	public Hamster(String nama, int panjang) {
		super(nama, panjang);
	}

	public void gnawing() {
		System.out.format("%s makes a voice: ngkkrit.. ngkkrrriiit \n", this.getNama());
		System.out.println("Back to the office!");
	}

	public void running() {
		System.out.format("%s makes a voice: trrr... trrr... \n", this.getNama());
		System.out.println("Back to the office!");
	}
}