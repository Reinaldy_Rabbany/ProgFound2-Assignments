import java.util.Scanner;

public class Parrot extends Animals {

	public Parrot(String nama, int panjang) {
		super(nama, panjang);
	}

	public void flyingParrot() {
		System.out.format("Parrot %s flies! \n", this.getNama());
		System.out.format("%s makes a voice: FLYYYY... \n", this.getNama());
		System.out.println("Back to the office!");
	}

	public void doConversation() {
		System.out.print("You say: ");
		Scanner speak = new Scanner(System.in);
		String speak2 = speak.nextLine();
		System.out.format("%s says: %s \n", this.getNama(), speak2.toUpperCase());
		System.out.println("Back to the office!");
	}
}