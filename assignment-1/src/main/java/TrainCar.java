public class TrainCar {

    WildCat cat;
    TrainCar next;

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // Method konstraktur Traincar, menerima satu parameter dengan bentuk objek WildCAt
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    // Method konstraktur Traincar, menerima dua parameter dengan bentuk objek WildCat dan TrainCar
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // Method untuk menghitung dan me-return hasil total berat seluruh TrainCar dan isinya melalui rekursif
    public double computeTotalWeight() {
        if (this.next == null) {
            return this.EMPTY_WEIGHT + this.cat.weight;
        }
        else {
            return this.EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight(); //Rekursif
        }
    }

    /* Method untuk menghitung dan me-return hasil total MassIndex 
       seluruh WildCat di dalam TrainCar melalui rekursif */
    public double computeTotalMassIndex() {
        if (this.next == null) {
            return this.cat.computeMassIndex();
        }
        else {
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex(); //Rekursif
        }
    }

    // Method untuk print ke layar secara deafult
    public void printCar() {
        if (this.next == null) {
            System.out.println("(" + this.cat.name.toUpperCase() + ")");
        }
        else {
            System.out.print("(" + this.cat.name.toUpperCase() + ")--");
            this.next.printCar();

        }
    }
}
