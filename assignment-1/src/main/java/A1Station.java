// Import kelas sesuai kebutuhan
import java.util.Scanner;
import java.util.ArrayList;

public class A1Station {

	// Atribut kelas A1Station
    private static final double THRESHOLD = 250; // in kilograms
    static WildCat cats;
    static TrainCar car;

    public static void main(String[] args) {
    	ArrayList<TrainCar> arrTrain = new ArrayList<TrainCar>(); // Membuat arraylist dengan jenis objek TrainCar
        Scanner input = new Scanner(System.in);
        String jmlhCat = input.nextLine();
        int jumlahCat = Integer.parseInt(jmlhCat);
        
        // Iterasi dengan for-loop sesuai dengan jumlah kucing yang diberikan
        for (int i = 0; i < jumlahCat; i++) {
        	int sisa = jumlahCat - i;
        	String CatsId = input.nextLine(); // input untuk atribut cats

        	String nameId = CatsId.split(",")[0]; // nama dari cats
        	double weightId = Double.parseDouble(CatsId.split(",")[1]); // berat dari cats
        	double lengthId = Double.parseDouble(CatsId.split(",")[2]); // panjang dari cats

        	cats = new WildCat(nameId, weightId, lengthId); // inisiasi WIldCat

        	if ((arrTrain.size() == 0 & sisa == 1) | arrTrain.size() == 0) {
        		car = new TrainCar(cats); // inisiasi TrainCar dengan satu parameter
        		arrTrain.add(car);

        		if (jumlahCat == 1 | sisa == 1) {
        			double avrg = (double)(arrTrain.get(arrTrain.size() - 1).computeTotalMassIndex() / arrTrain.size()); // avrg = rata-rata dari totalMassIndex

        			System.out.println("The train departs to Javari Park");
        			System.out.print("[LOCO]<--");
       				arrTrain.get(0).printCar();
       				System.out.print("Average mass index of all cats: ");
       				System.out.format("%.2f%n", avrg);

       				if (avrg >= 25 & avrg < 30) {
       					System.out.println("In average, the cats in the train are *overweight*");
       				}
       				else if (avrg >= 18.5 & avrg < 25) {
       					System.out.println("In average, the cats in the train are *normal*");
       				}
        			else if (avrg < 18.5) {
        				System.out.println("In average, the cats in the train are *underweight*");
        			}
       				else {
       					System.out.println("In average, the cats in the train are *obese*");
       				}

       				arrTrain.clear(); // mengkosongkon track pada arraylist arrTrain
        		}
        	}
        	else {
        		car = new TrainCar(cats, car); // inisiasi TrainCar dengan dua parameter
        		arrTrain.add(car);

        		if ((arrTrain.get(arrTrain.size() - 1).computeTotalWeight() > THRESHOLD & arrTrain.size() > 1) | sisa == 1) { 
        			double avrg = (double)(arrTrain.get(arrTrain.size() - 1).computeTotalMassIndex() / arrTrain.size());
        			System.out.println("The train departs to Javari Park");
        			System.out.print("[LOCO]<--");
        			arrTrain.get(arrTrain.size() - 1).printCar(); // melaksanakan perintah printCar dari train terakhir
        			System.out.print("Average mass index of all cats: ");
        			System.out.format("%.2f%n", avrg);

        			if (avrg >= 25 & avrg < 30) {
        				System.out.println("In average, the cats in the train are *overweight*");
        			}
        			else if (avrg >= 18.5 & avrg < 25) {
        				System.out.println("In average, the cats in the train are *normal*");
        			}
        			else if (avrg < 18.5) {
        				System.out.println("In average, the cats in the train are *underweight*");
        			}
        			else {
        				System.out.println("In average, the cats in the train are *obese*");
        			}
        			arrTrain.clear();
        		}
        	}
        }
    }
}
