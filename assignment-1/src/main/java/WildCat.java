public class WildCat {
    // Atribut kelas WildCat
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    // Method konstraktur kelas WildCat
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // Method untuk menghitung Mass Index WildCat
    public double computeMassIndex() {
        double MassIndex;
        MassIndex = this.weight / (this.length * this.length * 0.01 * 0.01); // Ubah length ke meter kubik
        return MassIndex;
    }
}
