import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import javax.imageio.*;

public class Board extends JFrame{

    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    String img = "/Users/Reinaldy/Desktop/ProgFound2-Assignments/assignment-4/img/icon1.png";
    ImageIcon defaultIcon = new ImageIcon(img);
    String defaultSrc = "/Users/Reinaldy/Desktop/ProgFound2-Assignments/assignment-4/img/";
    List<String> imgSrc = Arrays.asList(
    	"bird1.png", "aligator.png", "snail.png", "bird2.png", "cat.png", "bull.png", "dolphin.png", "elephant.png", "horse.png", 
    	"Scrat.png", "Scrat2.png", "lion.png", "python.png", "dog.png", "turtle.png", "dodo.png", "butterfly.png", "panda.png");


    public Board() {

        int pairsOfCard = 18;
        List<Card> cardsList = new ArrayList<Card>();
        List<Integer> cardVals = new ArrayList<Integer>();

        for (int i = 0; i < pairsOfCard; i++){
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);

        for (int val : cardVals){
            Card c = new Card();
            c.setIcon(new ImageIcon(img));
            c.setId(val);
            c.setDisabledIcon(new ImageIcon(defaultSrc + imgSrc.get(val)));
            c.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    c.setEnabled(false);
                    doTurn();
                }
            });
            cardsList.add(c);
        }
        this.cards = cardsList;

        //set up the board
        Container pane = getContentPane();
        pane.setLayout(new GridLayout(7, 6));
        for (Card c : cards){
            pane.add(c);
        }
        setTitle("Memory Match");

        JPanel p = new JPanel();

        JButton exit = new JButton("exit");
        exit.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e) {
        		System.exit(0);
        	}
        });

        JButton restart = new JButton("restart");
        restart.addActionListener(new ActionListener() {
  			public void actionPerformed(ActionEvent evt) {
  				for (Card card1 : cardsList) {
  					card1.setIcon(new ImageIcon(img));
  					card1.setEnabled(true);
  					card1.setVisible(true);
  					card1.setMatched(false);
  				}

  				c1 = null;
  				c2 = null;

  				Collections.shuffle(cardsList);
  				for (Card c : cardsList) {
  					pane.add(c, BorderLayout.SOUTH);
  				}
  			}
		});

        p.add(exit);
        p.add(restart);
        pane.add(p, BorderLayout.SOUTH);
    }

    public void doTurn(){
        if (c1 == null && c2 == null){
            c1 = selectedCard;
        } 
        else if (c1 != null  && c2 == null){
            c2 = selectedCard;
            checkCards();
        } 
        else if (c1 != null && c2 != null) {
        	c1.setEnabled(true);
        	c2.setEnabled(true);
        	c1 = selectedCard;
        	c2 = null;
        }
    }

    public void checkCards(){
        if (c1.getId() == c2.getId()){//match condition
            c1.setVisible(false); //disable to view the button
            c2.setVisible(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()){
                JOptionPane.showMessageDialog(this, "Congratulation" + "\nYou win!");
                System.exit(0);
            }
        }
    }

    public boolean isGameWon(){
        for(Card c: this.cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }
}