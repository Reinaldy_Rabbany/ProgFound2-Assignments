import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.nio.charset.StandardCharsets;

import javari.animal.*;
import javari.reader.ReaderPath;
import javari.park.*;

public class A3Festival {
	public static void main(String[] args) throws IOException {
		ArrayList<Visitor> arrOfVisitor = new ArrayList<>();
		ReaderPath categories, attractions, animalRecords;

		System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
		System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");
		System.out.print("Please provide the source data path: ");

		Scanner dataPath = new Scanner(System.in);
		String strDataPath = dataPath.nextLine();

		System.out.println();

		String dpAnimalsCategories = strDataPath + "/animals_categories.csv";
		Path path1 = Paths.get(dpAnimalsCategories);
		categories = new ReaderPath(path1);

		String dpAnimalsAttractions = strDataPath + "/animals_attractions.csv";
		Path path2 = Paths.get(dpAnimalsAttractions);
		attractions = new ReaderPath(path2);

		String dpAnimalsRecords = strDataPath + "/animals_records.csv";
		Path path3 = Paths.get(dpAnimalsRecords);
		animalRecords = new ReaderPath(path3);

		attractions.generateInput();

		categories.generateInput();

		animalRecords.setAttractions(attractions.getAttractions());
        animalRecords.setCategories(categories.getCategories());

        animalRecords.generateInput();

        for(Animal animal : animalRecords.getRecords()){
            if(animal.isShowable()){
                for(Attraction attraction : animalRecords.getAttractions()){
                    for(String type : attraction.getTypes()){
                        if(animal.getType().equals(type)){
                            attraction.addPerformer(animal);
                        }
                    }
                }
            }
        }

        System.out.println("... Loading... Success... System is populating data...\n");

        System.out.println("Found " + categories.getCategories().size() + " valid sections and " + 0 + " invalid sections");
        System.out.println("Found " + attractions.countValidRecords() + " valid attractions and " + attractions.countInvalidRecords() + " invalid sections");
        System.out.println("Found " + categories.countValidRecords() + " valid categories and " + categories.countInvalidRecords() + " invalid sections");
        System.out.println("Found " + animalRecords.countValidRecords() + " valid animal records and " + animalRecords.countInvalidRecords() + " invalid sections\n");

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");

        while (true) {
        	try {
        		int index = 1;
	            System.out.print("Javari Park has "+ animalRecords.getCategories().size() +" sections:\n");
	            for(Section sect1 :animalRecords.getCategories()){
	                System.out.println(index + ". " + sect1.getSectionName());
	                index++;
	            }
	            System.out.print("Please choose your preferred section (type the number): ");
	            Scanner input = new Scanner(System.in);
	            int chooseSection = input.nextInt();

	            index=1;
	            System.out.println("\n--" + animalRecords.getCategories().get(chooseSection-1).getSectionName() + "--");
	            for(String type : animalRecords.getCategories().get(chooseSection-1).getType()){
	                System.out.println(index + ". " + type);
	                index++;
	            }
	            System.out.print("Please choose your preferred animals (type the number): ");
	            Scanner input2 = new Scanner(System.in);
	            String chooseAnimal = input2.nextLine();

	            if(chooseAnimal.equals("#")) continue;

	            String animalType = animalRecords.getCategories().get(chooseSection-1).getType().get((Integer.parseInt(chooseAnimal))-1);

	            boolean availability = false;
	            for(Attraction attraction : animalRecords.getAttractions()){
	                for(String type : attraction.getTypes()){
	                    if(animalType.equals(type)){
	                        if(attraction.getPerformers().size() > 0){
	                            availability = true;
	                        }
	                    }
	                }
	            }
	            if(!availability){
	                System.out.println("\nUnfortunately, no " + animalType + " can perform any attraction, please choose other animals\n");
	                continue;
	            }


	            System.out.println("\n---" + animalType + "---\n" +
	                                "Attractions by " + animalType + ":");

	            index=1;
	            ArrayList<Attraction> option = new ArrayList<>();
	            for(Attraction attraction : animalRecords.getAttractions()){
	                for(String type : attraction.getTypes()){
	                    if(animalType.equals(type)){
	                        option.add(attraction);
	                        System.out.println(index + ". " + attraction.getName());
	                        index++;
	                    }
	                }
	            }

	            System.out.print("Please choose your preferred attractions (type the number): ");
	            Scanner input3 = new Scanner(System.in);
	            String chooseAttraction = input3.nextLine();

	            if(chooseAttraction.equals("#")) continue;

	            Attraction chosenAttraction = option.get(Integer.parseInt(chooseAttraction)-1);

	            System.out.print("\nWow, one more step,\n" +
	                    "please let us know your name: ");
	            Scanner input4 = new Scanner(System.in);
	            String visitorName = input4.nextLine();

	            System.out.print("\nYeay, final check!\n" +
	                    "Here is your data, and the attraction you chose:\n" +
	                    "Name: " + visitorName + "\n" +
	                    "Attractions: " + chosenAttraction.getName() + " -> " + animalType + "\n" +
	                    "With: ");

	            int i = 0;
	            String str = "";
	            for(Animal performers : chosenAttraction.getPerformers()){
	                if(performers.getType().equals(animalType)){
	                    str += chosenAttraction.getPerformers().get(i).getName();
	                    str += ", ";
	                }
	                i++;
	            }
	            System.out.println(str.substring(0, str.length() - 2));

	            System.out.println("\n");
	            System.out.print("Is the data correct? (Y/N): ");
	            Scanner input5 = new Scanner(System.in);
	            String confirmation = input5.nextLine();
	            if(confirmation.equalsIgnoreCase("N")){
	                System.out.println("Unregistering ticket. Back to main menu.\n");
	                continue;
	            }

	            boolean alreadyRegister = false;
	            for(Visitor visitor : arrOfVisitor){
	                if(visitor.getVisitorName().equals(visitorName)){
	                    visitor.addSelectedAttraction(chosenAttraction);
	                    alreadyRegister = true;
	                }
	            }
	            if(!alreadyRegister) arrOfVisitor.add(new Visitor(visitorName,chosenAttraction));
	            System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
	            Scanner input6 = new Scanner(System.in);
	            String anotherTicket = input6.nextLine();

	            if(anotherTicket.equalsIgnoreCase("Y")) continue;
	            System.out.println("\n... End of program");
	            break;
        	}
        	catch (IndexOutOfBoundsException a) {
        		System.out.println("\nincorrect input\n");
        		continue;
        	}
        }
	}
}