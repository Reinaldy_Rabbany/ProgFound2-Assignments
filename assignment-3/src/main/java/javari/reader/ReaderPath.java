package javari.reader;

import javari.animal.*;
import javari.park.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class ReaderPath extends CsvReader {

    private ArrayList<Attraction> attractions = new ArrayList<>();
    private ArrayList<Section> categories = new ArrayList<>();
    private List<Animal> records = new ArrayList<>();

    public ReaderPath(Path file) throws IOException{
        super(file);
    }

    public List<Animal> getRecords() {
        return records;
    }
    public void setRecords(List<Animal> records) {
        this.records = records;
    }

    public ArrayList<Attraction> getAttractions() {
        return attractions;
    }
    public void setAttractions(ArrayList<Attraction> attractions) {
        this.attractions = attractions;
    }

    public ArrayList<Section> getCategories() {
        return categories;
    }
    public void setCategories(ArrayList<Section> categories) {
        this.categories = categories;
    }

    public void generateInput(){
        for(int i=0;i<this.lines.size();i++){
            boolean getInside = false;
            String[] arr = lines.get(i).split(COMMA);

            if(arr.length==2){
                for(Attraction attraction: attractions){
                    if(attraction.getName().equals(arr[1])){
                        getInside = true;
                        attraction.addTypes(arr[0]);
                        break;
                    }
                }
                if(!getInside) attractions.add(new Attraction(arr[0],arr[1]));
            }

            else if(arr.length==3){
                for(int j=0;j<categories.size();j++){
                    if(categories.get(j).getSectionName().equals(arr[2])){
                        getInside = true;
                        categories.get(j).addType(arr[0]);
                        break;
                    }
                }
                if(!getInside) {
                    categories.add(new Section(arr[0], arr[1], arr[2]));
                }
            }

            else if(arr.length==8){
                String sectionName = "";
                for(Section x : categories){
                    for(String type : x.getType()){
                        if(type.equals(arr[1])){
                            sectionName = x.getSectionName();
                        }
                    }
                }
                if(sectionName.equals("Explore the Mammals")){
                    records.add(new Mammals(Integer.parseInt(arr[0]),arr[1],arr[2],Gender.parseGender(arr[3]),
                            Double.parseDouble(arr[4]),Double.parseDouble(arr[5]),arr[6],Condition.parseCondition(arr[7])));
                }
                else if(sectionName.equals("World of Aves")){
                    records.add(new Aves(Integer.parseInt(arr[0]),arr[1],arr[2],Gender.parseGender(arr[3]),
                            Double.parseDouble(arr[4]),Double.parseDouble(arr[5]),arr[6],Condition.parseCondition(arr[7])));
                }
                else if(sectionName.equals("Reptillian Kingdom")){
                    records.add(new Reptiles(Integer.parseInt(arr[0]),arr[1],arr[2],Gender.parseGender(arr[3]),
                            Double.parseDouble(arr[4]),Double.parseDouble(arr[5]),arr[6],Condition.parseCondition(arr[7])));
                }
            }
        }
    }


    @Override
    public long countValidRecords() {
        if(records.size()!=0) return (long) records.size();
        else if(attractions.size()!=0) return (long) attractions.size();
        return (long)categories.size();
    }

    @Override
    public long countInvalidRecords() {
        return 0;
    }
}
