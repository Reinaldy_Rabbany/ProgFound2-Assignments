package javari.park;

import java.util.ArrayList;

public class Section {

    private ArrayList<String> types = new ArrayList<>();
    private String category;
    private String name;

    public Section(String type, String category, String name){
        this.category = category;
        this.name = name;
        this.types.add(type);
    }

    public void setType(ArrayList<String> types) {
        this.types = types;
    }
    public ArrayList<String> getType() {
        return types;
    }
    public void addType(String type){
        this.types.add(type);
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public String getSectionName() {
        return name;
    }
    public void setSectionName(String name) {
        this.name = name;
    }
}
