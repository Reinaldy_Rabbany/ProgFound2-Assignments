package javari.park;


import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration{

    private String name;
    private int id;
    private List<SelectedAttraction> attractions = new ArrayList<>();

    public Visitor(String name, Attraction attraction){
        this.name = name;
        this.attractions.add(attraction);
    }

    public int getRegistrationId() {
        return this.id;
    }

    public String getVisitorName() {
        return this.name;
    }

    public String setVisitorName(String name) {
        String oldName = this.name;
        this.name = name;
        return oldName + " changed to" + name;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return this.attractions;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if(selected==null) return false;
        attractions.add(selected);
        return true;
    }
}
