package javari.park;

import javari.animal.*;

import java.util.*;

public class Attraction implements SelectedAttraction{

    private String name;
    private ArrayList<String> types = new ArrayList<>();
    private List<Animal> performers = new ArrayList<>();
    public Attraction(String type, String name){
        this.name = name;
        this.types.add(type);
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        String result = "";
        for(int i=0;i<types.size();i++){
            result+=types.get(i);
            if(i!=types.size()-1)result+=", ";
        }
        return result;
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    public boolean addPerformer(Animal performer) {
        if(performer == null) return false;
        performers.add(performer);
        return true;
    }

    public void addTypes(String type){
        this.types.add(type);
    }
    
    public ArrayList<String> getTypes() {
        return types;
    }
}