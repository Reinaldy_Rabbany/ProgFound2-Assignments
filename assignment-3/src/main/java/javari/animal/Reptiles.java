package javari.animal;

public class Reptiles extends Animal {

	public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialCondition, Condition condition) {
		super(id, type, name, gender, length, weight, specialCondition, condition);
	}

	protected boolean specificCondition() {
		if (this.getSpecialCondition().equalsIgnoreCase("wild")) {
			return false;
		}
		return true;
	}
}