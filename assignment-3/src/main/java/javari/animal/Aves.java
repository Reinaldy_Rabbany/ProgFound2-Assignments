package javari.animal;

public class Aves extends Animal {

	public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialCondition, Condition condition) {
		super(id, type, name, gender, length, weight, specialCondition, condition);
	}

	protected boolean specificCondition() {
		if (this.getSpecialCondition().equalsIgnoreCase("laying eggs")) {
			return false;
		}
		return true;
	}
}