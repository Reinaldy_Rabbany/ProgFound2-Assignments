package javari.animal;

public class Hamster extends Mammals {

	public Hamster(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialCondition, Condition condition) {
		super(id, type, name, gender, length, weight, specialCondition, condition);
	}
}